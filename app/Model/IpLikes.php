<?php namespace App\Model;
/**
 * @author: Aleksei Proskurnov <proskurn@gmail.com>
 * @copyright Copyright (c) 2016, Aleksei Proskurnov
 */

class IpLikes
{
    public $table = 'likes';
    private $db;

    public function __construct( $db )
    {
        $this->db = $db;
    }

    public function getUniqueForReview( $ip, $review_id )
    {
        $sql = "SELECT * FROM " . $this->table . " WHERE ip = ? AND review_id = ?";
        $row = $this->db->fetchAssoc($sql, [$ip, $review_id]);
        return $row;
    }

    public function create( $ip, $author_id )
    {
        $this->db->insert($this->table, [
            'ip'        =>  $ip,
            'review_id' =>  $author_id
        ]);
    }

}