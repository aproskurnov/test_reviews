<?php namespace App\Model;
/**
 * @author: Aleksei Proskurnov <proskurn@gmail.com>
 * @copyright Copyright (c) 2016, Aleksei Proskurnov
 */
use App\Model\IpLikes;
use Symfony\Component\Validator\Constraints\Ip;

class Review
{
    public $table = 'review';
    private $db;

    public function __construct( $db )
    {
        $this->db = $db;
    }

    public function getAll( $order, $direction )
    {
        $sql = 'SELECT * FROM ' . $this->table;

        if (($order === 'likes') || ($order === 'date')){
            if ($direction != 'DESC'){
                $direction = 'ASC';
            }
            $sql .= ' ORDER BY ' . $order . ' ' . $direction;

        }

        $rows = $this->db->fetchAll($sql);
        return $rows;
    }

    public function get( $id )
    {
        $sql = "SELECT * FROM " . $this->table . " WHERE id = ?";
        $row = $this->db->fetchAssoc($sql, [$id]);
        return $row;
    }

    public function create( $params )
    {
        $this->db->insert($this->table, $params);
    }

    public function like( $id, $ip )
    {
        $row = $this->get($id);

        $ipLikes = new IpLikes( $this->db );
        $like = $ipLikes->getUniqueForReview( $ip, $id );

        if ($like)
            return;

        $ipLikes->create($ip, $id);

        $sql = "UPDATE " . $this->table . " SET likes = ? WHERE id = ?";
        $this->db->executeUpdate($sql, [++$row['likes'], $id]);
    }

    public function getPrev( $id )
    {
        $row = $this->get($id);

        $sql = "select * from " . $this->table . " r where r.date = ? and r.id < ? order by id DESC limit 1;";
        $row1 = $this->db->fetchAssoc($sql, [$row['date'], $id]);
        if ($row1)
            return $row1;

        $sql = "select * from " . $this->table . " r where r.date < ? limit 1;";

        $row2 = $this->db->fetchAssoc($sql, [$row['date']]);
        return $row2;
    }

    public function getNext( $id )
    {
        $row = $this->get($id);

        $sql = "select * from " . $this->table . " r where r.date = ? and r.id > ? limit 1;";
        $row1 = $this->db->fetchAssoc($sql, [$row['date'], $id]);
        if ($row1)
            return $row1;

        $sql = "select * from " . $this->table . " r where r.date > ? limit 1;";
        $row2 = $this->db->fetchAssoc($sql, [$row['date']]);
        return $row2;
    }

}