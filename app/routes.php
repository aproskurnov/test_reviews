<?php
/**
 * @author: Aleksei Proskurnov <proskurn@gmail.com>
 * @copyright Copyright (c) 2016, Aleksei Proskurnov
 */

$app->mount("/", new App\Controller\ReviewController());