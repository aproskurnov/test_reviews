<?php namespace App\Controller;

/**
 * @author: Aleksei Proskurnov <proskurn@gmail.com>
 * @copyright Copyright (c) 2016, Aleksei Proskurnov
 */

use App\Model\Review;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints as Assert;

class ReviewController implements ControllerProviderInterface
{
    public function index( Application $app, Request $request ){

        $order = $request->get('order');
        $direction = $request->get('direction');

        $review = new Review($app['db']);
        $data = $review->getAll( $order, $direction );

        return $app['twig']->render('review/index.twig', [
            'reviews' =>  $data,
            'order' =>  $order,
            'direction' =>  $direction
        ]);
    }

    public function view( Application $app, $id ){
        $review = new Review($app['db']);
        $data = $review->get($id);
        $prev = $review->getPrev($id);
        $next = $review->getNext($id);
        return $app['twig']->render('review/view.twig', [
            'review'=>$data,
            'prev'  =>  $prev,
            'next'  =>  $next
        ]);
    }

    public function create( Application $app ){

        $form = $this->buildCreateForm( $app );
        return $app['twig']->render('review/create.twig',[
            'form' => $form->createView()
        ]);
    }

    public function store( Application $app, Request $request ){

        $form = $this->buildCreateForm( $app );
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $review = new Review( $app['db'] );
            $review->create([
                'author'  =>  strip_tags($data['author']),
                'text'  =>  strip_tags($data['text']),
                'ip'    =>  $request->server->get('REMOTE_ADDR'),
                'date'  =>  date('Y-m-d', time())
            ]);

            return $app->redirect('/', 301);
        }

        return $app['twig']->render('review/create.twig',[
            'form' => $form->createView()
        ]);
    }

    public function like( Application $app, Request $request, $id ){

        $order = $request->get('order');
        $direction = $request->get('direction');

        $url = '/';
        if ($order){
            $url .= '?order=' . $order;
            if ($direction){
                $url .= '&direction=' . $direction;
            }
        }

        $ip = $request->server->get('REMOTE_ADDR');

        $review = new Review($app['db']);
        $review->like($id, $ip);
        return $app->redirect($url, 301);
    }

    public function connect(Application $app){
        $reviews = $app['controllers_factory'];
        $reviews->get("/",'App\Controller\ReviewController::index');
        $reviews->get("/review/view/{id}",'App\Controller\ReviewController::view');
        $reviews->get("/review/create",'App\Controller\ReviewController::create');
        $reviews->post("/review/store",'App\Controller\ReviewController::store');
        $reviews->post("/review/like/{id}",'App\Controller\ReviewController::like');
        return $reviews;
    }

    private function buildCreateForm( $app )
    {
        return $app['form.factory']->createBuilder('form')
            ->add('author', null, [
                'label'=>'Ваше имя:',
                'constraints' => array(new Assert\NotBlank()),
                'attr'  =>  ['class'=>'block']
            ])
            ->add('text', TextareaType::class, [
                'label'=>'Текст:',
                'constraints' => array(new Assert\NotBlank()),
                'attr'  =>  ['class'=>'block']
            ])
            ->getForm();
    }
}